﻿using System.Collections.Generic;

namespace contratos.ClasesAyuda
{
    public class RespuestaBase
    {
        public RespuestaBase()
        {
            Errores = new List<Error>();
        }
        public bool Valida { get; set; }
        public int CantidadRegistros { get; set; }
        public List<Error> Errores { get; set; }
        public void AgregarError(string clave, string mensaje)
        {
            Errores.Add(new Error() { Clave = clave, Valor = mensaje });
        }
    }

    public class Error
    {
        public Error()
        {
            this.Clave = string.Empty;
        }

        public string Clave { get; set; }
        public string Valor { get; set; }
    }
}
