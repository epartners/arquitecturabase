﻿using System.Linq;
using System.Security.Claims;

namespace contratos.ClasesAyuda
{
    public class DatosUsuario
    {
        public string IdAuth0
        {
            get { return ClaimsPrincipal.Current.Claims.Where(cp => cp.Type == "auth0_id").Select(ct => ct.Value).SingleOrDefault(); }
        }

        public string NombreUsuario
        {
            get { return ClaimsPrincipal.Current.Claims.Where(cp => cp.Type == "fullname").Select(ct => ct.Value).SingleOrDefault(); }
        }

        public string Correo
        {
            get { return ClaimsPrincipal.Current.Claims.Where(cp => cp.Type == "email").Select(ct => ct.Value).SingleOrDefault(); }
        }

        public string URLImagen
        {
            get { return ClaimsPrincipal.Current.Claims.Where(cp => cp.Type == "picture").Select(ct => ct.Value).SingleOrDefault(); }
        }


    }
}
