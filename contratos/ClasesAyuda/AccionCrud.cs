﻿namespace contratos.ClasesAyuda
{
    public enum AccionCrud
    {
        Crear = 1,
        Editar = 2,
        Eliminar = 3,
        Obtener = 4
    }
}
