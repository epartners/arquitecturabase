﻿using System.Collections.Generic;

namespace contratos.ClasesAyuda
{
    public class ParametrosTabla
    {
        public List<string> Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public int Offset { get; set; }

        public int Limit { get; set; }

        public ModeloFiltro ModeloFiltro { get; set; }

    }
}
