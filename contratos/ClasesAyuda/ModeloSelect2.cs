﻿namespace contratos.ClasesAyuda
{
    public class ModeloSelect2
    {
        public string Id { get; set; }

        public string Valor { get; set; }

        public string Texto { get; set; }
    }
}
