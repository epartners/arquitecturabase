﻿namespace contratos.ClasesAyuda
{
    public abstract class SolicitudBase
    {
        //public DatosUsuario UsuarioActual => new DatosUsuario();
        /// <summary>
        /// Action requerida para el servicio
        /// </summary>
        public AccionCrud accionCrud { get; set; }

        /// <summary>
        /// Query usada por bootstrap table para filtrar
        /// </summary>
        public ParametrosTabla parametrosTabla { get; set; }
    }
}
