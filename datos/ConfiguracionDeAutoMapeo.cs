﻿using System;
using FluentNHibernate.Automapping;
using nucleo.Infraestructura;

namespace datos
{
    /// <summary>
    /// Configuración de automapeo donde se indentifica que clase de sebe mapear
    /// </summary>
    public class ConfiguracionDeAutoMapeo : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type tipo)
        {
            return tipo.IsSubclassOf(typeof(EntidadBase));
        }
    }
}
