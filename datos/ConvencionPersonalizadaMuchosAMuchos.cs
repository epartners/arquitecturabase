﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Inspections;

namespace datos
{
    public class ConvencionPersonalizadaMuchosAMuchos : ManyToManyTableNameConvention
    {
        protected override string GetBiDirectionalTableName(IManyToManyCollectionInspector coleccion, IManyToManyCollectionInspector otraTabla)
        {
            return coleccion.EntityType.Name + "Tiene" + otraTabla.EntityType.Name;
        }

        protected override string GetUniDirectionalTableName(IManyToManyCollectionInspector coleccion)
        {
            return coleccion.EntityType.Name + "Tiene" + coleccion.ChildType.Name;
        }
    }
}
