﻿
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using nucleo.Infraestructura;

namespace datos.Infraestructura
{
    public class UnidadDeTrabajo : IUnidadDeTrabajo
    {
        private static readonly ISessionFactory _fabricarSesion;
        private ITransaction _transaccion;

        public ISession Sesion {get; set; }

        static UnidadDeTrabajo()
        {
            _fabricarSesion = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(x => x.FromConnectionStringWithKey("BaseDatos")))
                .Mappings(a => a.AutoMappings.Add(AutoMap.AssemblyOf<EntidadBase>()
                    .Conventions.AddFromAssemblyOf<ConvencionPersonalizadaClaveForanea>()
                    .Conventions.AddFromAssemblyOf<ConvencionPersonalizadaMuchosAMuchos>()
                    .UseOverridesFromAssemblyOf<ConfiguracionDeAutoMapeo>()
                    .IgnoreBase(typeof(EntidadBase))))
                .ExposeConfiguration(config => new SchemaUpdate(config).Execute(false, true))
                .BuildSessionFactory();
        }

        public UnidadDeTrabajo()
        {
            Sesion = _fabricarSesion.OpenSession();
        }

        public void ComenzarTransaccion()
        {
            _transaccion = Sesion.BeginTransaction();
        }

        public void Deshacer()
        {
            try
            {
                if (_transaccion != null && _transaccion.IsActive) _transaccion.Rollback();
            }
            finally
            {
                Sesion.Dispose();
            }

        }

        public void Persistir()
        {
            try
            {
                if (_transaccion != null && _transaccion.IsActive) _transaccion.Commit();
            }
            catch
            {
                if (_transaccion != null && _transaccion.IsActive) _transaccion.Rollback();
            }
            finally
            {
                Sesion.Dispose();
            }
        }
    }
}
