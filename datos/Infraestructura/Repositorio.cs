﻿using NHibernate;
using NHibernate.Linq;
using nucleo.Infraestructura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace datos.Infraestructura
{
    public class Repositorio<T> : IRepositorio<T> where T : EntidadBase
    {
        private readonly UnidadDeTrabajo _unidadDeTrabajo;

        public Repositorio(IUnidadDeTrabajo unidadDeTrabajo)
        {
            _unidadDeTrabajo = (UnidadDeTrabajo)unidadDeTrabajo;
        }

        protected ISession Sesion => _unidadDeTrabajo.Sesion;

        public async Task Actualizar(T entidad)
        {
            await Sesion.UpdateAsync(entidad);

            try
            {
                Sesion.Flush();
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<IEnumerable<T>> Buscar(Expression<Func<T, bool>> predicado)
        {
            return await Sesion.Query<T>().Where(predicado).ToListAsync().ConfigureAwait(false);
        }

        public async Task Eliminar(T entidad)
        {
            await Sesion.UpdateAsync(entidad);
        }

        public async Task Insertar(T entidad)
        {
            await Sesion.SaveAsync(entidad);
            
        }
    
        public async Task<T> Obtener(int id)
        {
            return await Sesion.GetAsync<T>(id);
        }

        public async Task<IEnumerable<T>> ObtenerTodos()
        {
            return await Sesion.Query<T>().ToListAsync();
         
        }

    }
}
