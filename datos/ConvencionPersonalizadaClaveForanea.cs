﻿using System;
using FluentNHibernate;
using FluentNHibernate.Conventions;

namespace datos
{
    public class ConvencionPersonalizadaClaveForanea : ForeignKeyConvention
    {
        protected override string GetKeyName(Member propiedad, Type tipo)
        {
            if (propiedad == null)
            {
                return tipo.Name + "Id";
            }
            return propiedad.Name + "Id";
        }
    }
}
