﻿namespace nucleo.Infraestructura
{
    /// <summary>
    /// Unidad de trabajo encargada de impactar en la base de datos y manejar las sesiones de NH
    /// </summary>
    public interface IUnidadDeTrabajo
    {
        /// <summary>
        /// Iniciar una transaccion
        /// </summary>
        void ComenzarTransaccion();

        /// <summary>
        /// Se persisten los cambios en la base de datos
        /// </summary>
        void Persistir();

        /// <summary>
        /// Se vuelven atras todos los cambios realizados
        /// </summary>
        void Deshacer();
    }
}
