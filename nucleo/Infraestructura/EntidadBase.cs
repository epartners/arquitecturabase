﻿using System;

namespace nucleo.Infraestructura
{
    /// <summary>
    /// Entidad base
    /// </summary>
    public class EntidadBase
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Identificador que nos indica el usuario que creo la entidad
        /// </summary>
        public virtual string CreadoPor { get; set; }

        /// <summary>
        /// Identificador que nos indica la fecha de creacion de la entidad
        /// </summary>
        public virtual DateTime FechaCreado { get; set; }

        /// <summary>
        /// Identificador que nos indica el ultimo usuario que actualizo la entidad
        /// </summary>
        public virtual string ActualizadoPor { get; set; }

        /// <summary>
        /// Identificador que nos indica la ultima fecha de actualizacion de la entidad
        /// </summary>
        public virtual DateTime? FechaActualizado { get; set; }

        /// <summary>
        /// Identificador que nos indica el usuario que borro la entidad
        /// </summary>
        public virtual string EliminadoPor { get; set; }

        /// <summary>
        /// Identificador que nos indica la fecha de borrado de la entidad
        /// </summary>
        public virtual DateTime? FechaEliminado { get; set; }
    }
}
