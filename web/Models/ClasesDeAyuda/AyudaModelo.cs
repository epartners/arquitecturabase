﻿using contratos.ClasesAyuda;
using System.Web.Http.ModelBinding;

namespace web.ClasesDeAyuda
{
    public class AyudaModelo
    {
        public static void MapearRespuesta(ModelStateDictionary modelo, RespuestaBase respuesta)
        {
            if (respuesta.Valida) return;

            foreach (var error in respuesta.Errores)
            {
                modelo.AddModelError(error.Clave, error.Valor);
            }
        }
    }
}