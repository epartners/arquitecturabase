﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace web.ClasesDeAyuda
{
    public class RespuestaJsonCamelCase : JsonResult
    {
        private static readonly JsonSerializerSettings Configuraciones = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new StringEnumConverter() }
        };

        public override void ExecuteResult(ControllerContext contexto)
        {
            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                string.Equals(contexto.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("El método GET no esta permitido.");
            }

            var respuesta = contexto.HttpContext.Response;

            respuesta.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
            {
                respuesta.ContentEncoding = ContentEncoding;
            }

            if (Data == null)
            {
                return;
            }

            respuesta.Write(JsonConvert.SerializeObject(Data, Configuraciones));
        }
    }
}