﻿using nucleo.Infraestructura;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace web.Filtros
{
    public class FiltroDeAccionParaUnidadDeTrabajo : ActionFilterAttribute
    {
        public IUnidadDeTrabajo UnidadDeTrabajo { get; set; }

        public override void OnActionExecuting(HttpActionContext contextoAccion)
        {
            UnidadDeTrabajo = contextoAccion.Request.GetDependencyScope().GetService(typeof(IUnidadDeTrabajo)) as IUnidadDeTrabajo;
            UnidadDeTrabajo?.ComenzarTransaccion();
        }

        public override void OnActionExecuted(HttpActionExecutedContext contextoAccionEjecutada)
        {
            if (contextoAccionEjecutada.Request.Method == HttpMethod.Get) return;

            UnidadDeTrabajo = contextoAccionEjecutada.Request.GetDependencyScope().GetService(typeof(IUnidadDeTrabajo)) as IUnidadDeTrabajo;
            if (contextoAccionEjecutada.Response.IsSuccessStatusCode)
            {
                UnidadDeTrabajo?.Persistir();
            }
            else
            {
                UnidadDeTrabajo?.Deshacer();
            }
        }
    }
}