﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace web.Filtros
{
    public class FiltroExcepcionApi : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext contexto)
        {
            contexto.Response = contexto.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, contexto.Exception.Message);
        }
    }
}