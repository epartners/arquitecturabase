﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace web.Filtros
{
    public class FiltroValidacionModelo : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext contextoAccion)
        {
            if (contextoAccion.Request.Method == HttpMethod.Get) return;

            if (contextoAccion.ActionArguments.Any(x => x.Value == null))
            {
                contextoAccion.Response = contextoAccion.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "El valor no puede estar vacío.");
            }

            if (!contextoAccion.ModelState.IsValid)
            {
                contextoAccion.Response = contextoAccion.Request.CreateErrorResponse(HttpStatusCode.BadRequest, contextoAccion.ModelState);
            }
        }
    }
}