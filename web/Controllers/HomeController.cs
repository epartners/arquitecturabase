﻿using System.Web.Mvc;

namespace web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController()
        {
            
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Inicio";
            return View();
        }

    }
}
