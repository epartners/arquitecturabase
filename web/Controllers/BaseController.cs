﻿using contratos.ClasesAyuda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using web.ClasesDeAyuda;

namespace web.Controllers
{
    public class BaseController : Controller
    {
        public DatosUsuario UsuarioActual { get; set; }

        public BaseController()
        {
            UsuarioActual = new DatosUsuario();
        }

        protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new RespuestaJsonCamelCase
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }
    }
}