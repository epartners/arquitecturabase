﻿using contratos.ClasesAyuda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using web.ClasesDeAyuda;

namespace web.Controllers.API
{
    public class BaseApiController : ApiController
    {
        public DatosUsuario UsuarioActual { get; set; }

        public BaseApiController()
        {
            UsuarioActual = new DatosUsuario();
        }



        public void MapearErrores(ModelStateDictionary modelState, RespuestaBase response)
        {
            AyudaModelo.MapearRespuesta(modelState, response);
        }
    }
}