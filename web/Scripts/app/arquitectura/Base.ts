﻿"use strict";

module Base {

    export class Controlador {

        static actual;

        static inicializar(tipoControlador: { new (datosControlador?) }, URLDatosIniciales?: string) {
            $("body").keydown(event => { if (event.keyCode === 13) return false; });

            $.blockUI();

            $.when(this.cargarDatosIniciales(URLDatosIniciales)).done((respuestaDatosDelControlador) => {

                var datosDelControlador = respuestaDatosDelControlador != null ? respuestaDatosDelControlador : null;

                var controlador = new tipoControlador(datosDelControlador);

                Controlador.registrarControlador(controlador);

                this.ValidacionesDeKnockout();

                ko.applyBindings(controlador);

                $.unblockUI();

                $("#app").removeClass("hide");
            });
        }

        static cargarDatosIniciales(datosIniciales: string): JQueryPromise<any> {

            if (!datosIniciales)
                return $.Deferred<any>().resolve().promise();

            return $.ajax(datosIniciales, {
                type: "GET",
                context: this,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error: (error) => {
                    this.manejarError(error);
                }
            });
        }

        static manejarError(jqxhr: JQueryXHR) {
            var respuesta = jqxhr.responseJSON;
            if (respuesta.modelState != undefined) {
                var errores = respuesta.modelState;
                for (var err in errores) {
                    if (errores.hasOwnProperty(err)) {

                        var titulo = err[0].toUpperCase() + err.substr(1);;
                        var mensajes = errores[err][0];
                        delete errores[err];

                        toastr.error(mensajes, titulo);
                    }
                }
            } else {
                toastr.error(respuesta.message);
            }
        }

        static registrarControlador(nuevoControlador: any) {
            Controlador.actual = nuevoControlador;
        }

        static ValidacionesDeKnockout() {
            ko.validation.init({
                decorateInputElement: true,
                errorElementClass: "has-error"
            });
            ko.validation.locale("es-ES");
        }
        
        mostrarErrores(jqxhr: JQueryXHR) {
            var respuesta = jqxhr.responseJSON;
            if (respuesta.modelState != undefined) {
                var errores = respuesta.modelState;
                for (var err in errores) {
                    if (errores.hasOwnProperty(err)) {

                        var titulo = err[0].toUpperCase() + err.substr(1);;
                        var mensaje = errores[err][0];
                        delete errores[err];

                        toastr.error(mensaje, titulo);
                    }
                }
            } else {
                toastr.error(respuesta.message);
            }
        }
    }
}