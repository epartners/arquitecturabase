"use strict";
var Base;
(function (Base) {
    var Controlador = /** @class */ (function () {
        function Controlador() {
        }
        Controlador.inicializar = function (tipoControlador, URLDatosIniciales) {
            var _this = this;
            $("body").keydown(function (event) { if (event.keyCode === 13)
                return false; });
            $.blockUI();
            $.when(this.cargarDatosIniciales(URLDatosIniciales)).done(function (respuestaDatosDelControlador) {
                var datosDelControlador = respuestaDatosDelControlador != null ? respuestaDatosDelControlador : null;
                var controlador = new tipoControlador(datosDelControlador);
                Controlador.registrarControlador(controlador);
                _this.ValidacionesDeKnockout();
                ko.applyBindings(controlador);
                $.unblockUI();
                $("#app").removeClass("hide");
            });
        };
        Controlador.cargarDatosIniciales = function (datosIniciales) {
            var _this = this;
            if (!datosIniciales)
                return $.Deferred().resolve().promise();
            return $.ajax(datosIniciales, {
                type: "GET",
                context: this,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error: function (error) {
                    _this.manejarError(error);
                }
            });
        };
        Controlador.manejarError = function (jqxhr) {
            var respuesta = jqxhr.responseJSON;
            if (respuesta.modelState != undefined) {
                var errores = respuesta.modelState;
                for (var err in errores) {
                    if (errores.hasOwnProperty(err)) {
                        var titulo = err[0].toUpperCase() + err.substr(1);
                        ;
                        var mensajes = errores[err][0];
                        delete errores[err];
                        toastr.error(mensajes, titulo);
                    }
                }
            }
            else {
                toastr.error(respuesta.message);
            }
        };
        Controlador.registrarControlador = function (nuevoControlador) {
            Controlador.actual = nuevoControlador;
        };
        Controlador.ValidacionesDeKnockout = function () {
            ko.validation.init({
                //insertMessages: false,
                decorateInputElement: true,
                errorElementClass: "has-error"
            });
            ko.validation.locale("es-ES");
        };
        Controlador.prototype.mostrarErrores = function (jqxhr) {
            var respuesta = jqxhr.responseJSON;
            if (respuesta.modelState != undefined) {
                var errores = respuesta.modelState;
                for (var err in errores) {
                    if (errores.hasOwnProperty(err)) {
                        var titulo = err[0].toUpperCase() + err.substr(1);
                        ;
                        var mensaje = errores[err][0];
                        delete errores[err];
                        toastr.error(mensaje, titulo);
                    }
                }
            }
            else {
                toastr.error(respuesta.message);
            }
        };
        return Controlador;
    }());
    Base.Controlador = Controlador;
})(Base || (Base = {}));
//# sourceMappingURL=Common.js.map