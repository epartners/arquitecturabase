"use strict";
var Modelos;
(function (Modelos) {
    var ModeloFiltro = /** @class */ (function () {
        function ModeloFiltro() {
            this.puestoId = ko.observable("");
            this.ConfiguracionSelect2 = {
                width: "100%",
                allowClear: true,
                placeholder: "Seleccione una opcion...",
                language: "es"
            };
            this.puestoId.subscribe(function () {
                $("#tabla-servidor").bootstrapTable("refresh");
            });
        }
        return ModeloFiltro;
    }());
    Modelos.ModeloFiltro = ModeloFiltro;
})(Modelos || (Modelos = {}));
//# sourceMappingURL=ModeloFiltro.js.map