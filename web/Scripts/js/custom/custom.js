﻿//Funcion que nos marca en el sidebar el menu donde estamos parados
$(function () {
    var url = window.location.pathname;
    var activePage = url.substring(url.lastIndexOf("/") + 1);

    $("#menu-icons li a").each(function () {
        var currentPage = this.href.substring(this.href.lastIndexOf("/") + 1);

        if ($(this).parent().hasClass("active")) {
            $(this).parent().removeClass("active");
        }

        if (activePage === currentPage) {
            $(this).parent().addClass("active");
        }
    });
});

//Iniciar bootstrap table
$("#tabla-servidor").bootstrapTable({
    pagination: true,
    search: true,
    clickToSelect: true,
    singleSelect: true,
    sidePagination: "server",
    pageSize: 10,
    toolbar: "#barraHerramientas",
    queryParams: function (params) {
        var filter = {};
       
        if (typeof Base !== "undefined")
            filter = Base.Controlador.actual.modelFiltro;

        var query = {
            limit: params.limit,
            offset: params.offset,
            order: params.order,
            search: params.search,
            sort: params.sort,
            filterModel: filter
        };

        return query;
    },
    onCheck: function (data, $element) {
        var tr = $($element).closest("tr");
        Base.Controlador.actual.seleccionaRegistro(data, tr);
    },
    onUncheck: function (data, $element) {
        var tr = $($element).closest("tr");
        Base.Controlador.actual.deseleccionaRegistro(data, tr);
    },
    onLoadError: function (status, jqxhr) {
        Base.Controlador.manejarError(jqxhr);
    },
    onRefresh: function () {
        Base.Controlador.actual.deseleccionaRegistro();
    }
});

$("#tabla-local").bootstrapTable({
    pagination: true,
    pageSize: 10,
    uniqueId: "id",
    toolbar: "#table-toolbar-local"
});

//Iniciamos todos los tooltip
$("[data-toggle='tooltip']").tooltip();

//Iniciar BlockUI con configuraciones propias
$.blockUI.defaults.css = {
    padding: 0,
    margin: 0,
    width: "30%",
    top: "40%",
    left: "35%",
    textAlign: "center",
    color: "#000",
    border: "none",
    backgroundColor: "none",
    cursor: "wait"
};
$.blockUI.defaults.baseZ = 1050;
$.blockUI.defaults.message = '<h3 style="color:#fff;"><i class="fa fa-refresh fa-spin fa-3x fa-fw" style="width: inherit"></i></h3>';