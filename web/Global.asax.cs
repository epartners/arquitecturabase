﻿//using contratos.InterfazServicios;
//using servicios;
using datos.Infraestructura;
using LightInject;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using nucleo.Infraestructura;
using System.Security.Claims;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(ConfiguracionWebApi.Register);
            ConfiguracionFiltros.RegisterGlobalFilters(GlobalFilters.Filters);
            ConfiguracionRutas.RegisterRoutes(RouteTable.Routes);
            ConfiguracionPaquetes.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            RegistrarContenedor();
        }

        private static void RegistrarContenedor()
        {
            var contenedor = new ServiceContainer();
            contenedor.EnableAnnotatedPropertyInjection();
            RegistrarInfrastructura(contenedor);
            RegistrarServicios(contenedor);
            RegistrarWebApi(contenedor);
            RegistrarMvc(contenedor);
        }

        private static void RegistrarInfrastructura(IServiceContainer contenedor)
        {
            contenedor.Register<IUnidadDeTrabajo, UnidadDeTrabajo>(new PerScopeLifetime());
            contenedor.Register(typeof(IRepositorio<>), typeof(Repositorio<>), new PerScopeLifetime());
        }

        private static void RegistrarServicios(IServiceRegistry contenedor)
        {
            /*  Registro de Servicios  
            contenedor.Register<IServicio, Servicio>(new PerScopeLifetime());
            */
        }

        private static void RegistrarWebApi(IServiceContainer contenedor)
        {
            contenedor.RegisterApiControllers();
            contenedor.EnablePerWebRequestScope();
            contenedor.EnableWebApi(GlobalConfiguration.Configuration);
        }

        private static void RegistrarMvc(IServiceContainer contenedor)
        {
            contenedor.RegisterControllers();
            contenedor.EnableMvc();
        }
    }
}
