﻿using System.Web.Http;
using web.Filtros;

namespace web
{
    public static class ConfiguracionWebApi
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web
            config.Filters.Add(new FiltroDeAccionParaUnidadDeTrabajo());
            config.Filters.Add(new FiltroExcepcionApi());
            config.Filters.Add(new FiltroValidacionModelo());
            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
