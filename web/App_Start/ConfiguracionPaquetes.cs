﻿using System.Web.Optimization;

namespace web
{
    public class ConfiguracionPaquetes
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/plugins/js").Include(
                "~/Scripts/js/jquery/jquery-3.2.1.js",
                "~/Scripts/js/bootstrap/bootstrap.js",
                "~/Scripts/js/bootstrap-table/bootstrap-table.js",
                "~/Scripts/js/bootstrap-table/bootstrap-table-es-AR.js",
                "~/Scripts/js/knockout/knockout-3.4.2.js",
                "~/Scripts/js/knockout-select2/knockout-select2.js",
                "~/Scripts/js/knockout-validation/knockout.validation.js",
                "~/Scripts/js/knockout-validation/es-ES.js",
                "~/Scripts/js/toastr/toastr.js",
                "~/Scripts/js/blockUI/jquery.blockUI.js",
                "~/Scripts/js/custom/custom.js"));


            bundles.Add(new StyleBundle("~/plugins/css").Include(
                "~/Content/css/bootstrap/bootstrap.css",
                "~/Content/css/toastr/toastr.css",
                "~/Content/css/custom/custom.css"));

            bundles.Add(new ScriptBundle("~/app/architecture").Include(
                "~/Scripts/app/arquitectura/Base.js"));


            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
        }
    }
}
