﻿using System.Web.Mvc;
using System.Web.Routing;

namespace web
{
    public class ConfiguracionRutas
    {
        public static void RegisterRoutes(RouteCollection rutas)
        {
            rutas.IgnoreRoute("{resource}.axd/{*pathInfo}");

            rutas.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
