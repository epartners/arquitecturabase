﻿using System.Web.Mvc;

namespace web
{
    public class ConfiguracionFiltros
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filtros)
        {
            filtros.Add(new HandleErrorAttribute());
        }
    }
}
